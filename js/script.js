$(document).ready(function()
{
	$('.slick-slider').slick({
		infinite: true,
		 autoplay: true,
        autoplaySpeed: 2000,
		arrows:false,
	    dots: true,
	    slidesToShow: 6.9,
	    slidesToScroll: 3.0,
	   
			
	});

}); 

$(document).ready(function()
{
	$('.slick-slider2').slick({
		infinite: true,
		 autoplay: true,
        autoplaySpeed: 2000,
		arrows:false,
	    dots: true,
	    slidesToShow: 3,
	    slidesToScroll: 1,
	   
			
	});

}); 

$(document).ready(function()
{
	$('.slick-slider3').slick({
		infinite: true,
		 autoplay: true,
        autoplaySpeed: 2000,
		arrows:false,
	    dots: true,
	    slidesToShow: 4,
	    slidesToScroll: 1,
	   
			
	});

}); 